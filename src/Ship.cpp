#include "Ship.h"
#include <iostream>
using namespace std;

Ship::Ship()
{
    set_parameters(0, 0, 0, 0, 0, 0);
}

Ship::~Ship()
{
    //dtor
}

/*******************
** PRINT SHIPS DATA **
*******************/
void Ship::print_data(){
    cout<<"id"<<id<<"  "
        <<"x"<<x<<"  "
        <<"y"<<y<<"  "
        <<"length"<<length<<"  "
        <<"damage"<<damage<<"  "
        <<"plane"<<plane<<endl;
}

/*******************
** SET PARAMETERS **
*******************/
void Ship::set_parameters(int id, int x, int y, int length_of_ship, int damage, int plane){
    this->id=id;
    this->x=x;
    this->y=y;
    this->length=length_of_ship;
    this->damage=damage;
    this->plane=plane;

}
/*******************
** INCREASE DAMAGE **
*******************/
void Ship::increase_damage(){
    damage++;
}

/*******************
** GET /  SET **
*******************/
int Ship::get_id(){
    return id;
}

void Ship::set_id(int id){
    this->id=id;
}
//////////////////////
int Ship::get_x(){
    return x;
}

void Ship::set_x(int x){
    this->x=x;
}
//////////////////////
int Ship::get_y(){
    return y;
}

void Ship::set_y(int y){
    this->y=y;
}
//////////////////////
int Ship::get_length(){
    return length;
}

void Ship::set_length(int length){
    this->length=length;
}
//////////////////////
int Ship::get_damage(){
    return damage;
}

void Ship::set_damage(int damage){
    this->damage=damage;
}
//////////////////////

int Ship::get_plane(){
    return plane;
}

void Ship::set_plane(int plane){
    this->plane=plane;
}
