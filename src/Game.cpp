#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <conio.h>
#include <time.h>

#include "Game.h"
#include "Board.h"
#include "Ship.h"

using namespace std;
int Game::how_many_hits_made_player = 0; //info - definition this static variable should be in upper part
int Game::how_many_hits_made_computer = 0;
/*******************
** CONSTRUCTOR **
*******************/
Game::Game()
{
    srand(time(NULL));

    char wybor;
    //player choosing visibility

    while(true){
        cout<<"Do you want to see your enemy`s ships [1 yes - 0 not)?"<<endl;
        wybor = getch();
        if(wybor == '1'){
            is_board_visible = true;
            break;
        }else if(wybor == '0'){
            is_board_visible = false;
            break;
        }
    }
    system("cls");

    //player set his board
    Game::set_player_ships_on_board(pPlayerBoard, pPlayerShipFragments);
    //Computer set his board
    Game::set_enemy_ships_on_board(pEnemyBoard, pEnemyShipFragments);

    Game::game_loop(pEnemyBoard, pEnemyShipFragments, pPlayerBoard, pPlayerShipFragments);

    cout<<"Game Over!"<<endl;
}

/*******************
** PRINT SHIPS DATA **
*******************/
void Game::print_ships_data(Ship *pEnemyShipFragments, int how_many_fragments){
    for(int i=0; i<how_many_fragments; i++){
        pEnemyShipFragments[i].print_data();
    }
}

/*******************
** GAME LOOP **
*******************/
void Game::game_loop(Board *pEnemyBoard, Ship *pEnemyShipFragments, Board *pPlayerBoard, Ship *pPlayerShipFragments){

   while(true){

        player_shooting(pEnemyBoard, pEnemyShipFragments);
        cout<<"how_many_hits_made_player"<<how_many_hits_made_player<<endl;
        if(how_many_hits_made_player == how_many_fragments){
            system("cls");
            cout<<"PLAYER IS WINNER!"<<endl;
            break;
        }

        cout<<"________________________"<<endl<<endl;
        computer_shooting(pPlayerBoard, pPlayerShipFragments);
        cout<<"how_many_hits_made_computer"<<how_many_hits_made_computer<<endl;
        if(how_many_hits_made_computer == how_many_fragments){
            system("cls");
            cout<<"COMPUTER IS WINNER!"<<endl;
            break;
        }
    }
}

/*******************
** PLAYER SHOOTING **
*******************/
void Game::player_shooting(Board *pEnemyBoard, Ship *pEnemyShipFragments){

//1 Print enemy`s board
    cout<<"Here is your enemy`s board"<<endl;
    pEnemyBoard->print_board(is_board_visible);
//2 Get coordinates to shooting
    cout<<"Podaj wspolrzedna x do ostrzalu [1-10]: " << endl;
    int temp_x = Game::get_cordinates((pEnemyBoard->get_length_of_board())-2);

    cout<<"Podaj wspolrzedna y do ostrzalu [1-10]: " << endl;
    int temp_y = Game::get_cordinates((pEnemyBoard->get_length_of_board())-2);

//3 Clean displayer
    system("cls");
//4 Check coordinates
    if(pEnemyBoard->get_field(temp_x,temp_y)=='8'){

        int which_fragment = detect_which_fragment_was_hited(temp_x, temp_y, pEnemyShipFragments, how_many_fragments);

        increase_damage(pEnemyShipFragments, pEnemyShipFragments[which_fragment].get_id());

        if(pEnemyShipFragments[which_fragment].get_damage() == pEnemyShipFragments[which_fragment].get_length()){
            cout<<"O LODZ ZATOPIONA!!"<<endl;
            exclude_positions(pEnemyBoard, pEnemyShipFragments, which_fragment, pEnemyShipFragments[which_fragment].get_id());

        }else
            cout<<"TRAFIONA PLYNIE!!"<<endl;

        pEnemyBoard->set_field(temp_x,temp_y,'X');
        how_many_hits_made_player++;
    }
    else if(pEnemyBoard->get_field(temp_x,temp_y)=='X' || pEnemyBoard->get_field(temp_x,temp_y)=='*'){
        cout<<"\tKomunikat: Kapitanie, zmarnowalismy rakiete, tu juz strzelalismy!"<<endl;
    }
    else{
        cout<<"\tKomunikat: Pudlo!!"<<endl;
        pEnemyBoard->set_field(temp_x,temp_y,'*');
    }
}

/*******************
** COMPUTER SHOOTING **
*******************/
void Game::computer_shooting(Board *pPlayerBoard, Ship *pPlayerShipFragments){

    int temp_x, temp_y;

    //!! looking for free place
    while(true){
        temp_x = rand() % 10+1; //random number in the range 1 - 10
        temp_y = rand() % 10+1;        if( pPlayerBoard->get_field(temp_x,temp_y) == '.')
            break;
        if( pPlayerBoard->get_field(temp_x,temp_y) == '8')
            break;
    }

    cout<<"Komputer wylosowal wspolrzedna x: " << temp_x << endl;
    cout<<"Komputer wylosowal wspolrzedna y: " << temp_y << endl;


// Check coordinates
    if(pPlayerBoard->get_field(temp_x,temp_y)=='8'){

        int which_fragment = detect_which_fragment_was_hited(temp_x, temp_y, pPlayerShipFragments, how_many_fragments);

        increase_damage(pPlayerShipFragments, pPlayerShipFragments[which_fragment].get_id());

        if(pPlayerShipFragments[which_fragment].get_damage() == pPlayerShipFragments[which_fragment].get_length()){
            cout<<"KOMPUTER ZATOPIL LODZ GRACZA!!"<<endl;
            exclude_positions(pPlayerBoard, pPlayerShipFragments, which_fragment, pPlayerShipFragments[which_fragment].get_id());

        }else
            cout<<"KOMPUTER TRAFIL ALE LODZ PLYNIE!!"<<endl;

        pPlayerBoard->set_field(temp_x,temp_y,'X');
        how_many_hits_made_computer++;
    }
    else{
        cout<<"\tKOMPUTER Pudluje!!"<<endl;
        pPlayerBoard->set_field(temp_x,temp_y,'*');
    }
    cout<<"Komputer widzi cos takiego po strzale: "<<endl;
    pPlayerBoard->print_board(1);
}

/***********************
** EXCLUDE POSITIONS  **
***********************/
void Game::exclude_positions(Board *pEnemyBoard, Ship *pEnemyShipFragments, int which_fragment, int id){

    int starting_x, starting_y;
    int orientation = pEnemyShipFragments[which_fragment].get_plane();

    for(int i=0; i<how_many_fragments; i++){
        if(pEnemyShipFragments[i].get_id() == id){
            starting_x = pEnemyShipFragments[i].get_x();
            starting_y = pEnemyShipFragments[i].get_y();
            break;
        }
    }

    if(orientation){
        for(int i=0; i<(2+pEnemyShipFragments[which_fragment].get_length()); i++){
            pEnemyBoard->set_field((starting_x-1),(starting_y-1+i),'*');
            pEnemyBoard->set_field((starting_x+1),(starting_y-1+i),'*');
            pEnemyBoard->set_field((starting_x),(starting_y-1),'*');
            pEnemyBoard->set_field((starting_x),(starting_y+(pEnemyShipFragments[which_fragment].get_length())),'*');
        }
    }else{
        for(int i=0; i<(2+pEnemyShipFragments[which_fragment].get_length()); i++){
            pEnemyBoard->set_field((starting_x-1+i),(starting_y-1),'*');
            pEnemyBoard->set_field((starting_x-1),(starting_y),'*');
            pEnemyBoard->set_field((starting_x+(pEnemyShipFragments[which_fragment].get_length())),(starting_y),'*');
            pEnemyBoard->set_field((starting_x-1+i),(starting_y+1),'*');
        }
    }
}

/*********************
** INCREASE DAMAGE  **
*********************/
void Game::increase_damage(Ship *pEnemyShipFragments, int id){

    for(int i=0; i<how_many_fragments; i++){
        if(pEnemyShipFragments[i].get_id() == id){
            pEnemyShipFragments[i].increase_damage();
        }
    }
}

/********************************
** DETECT WHICH SHIP WAS HITED **
********************************/
int Game::detect_which_fragment_was_hited(int x, int y, Ship *pEnemyShipFragments, int how_many_fragments){
    int i=0;
    for(i=0; i<how_many_fragments; i++){
        if((pEnemyShipFragments[i].get_x() == x) &&
           (pEnemyShipFragments[i].get_y() == y))
            break;
    }
    return i;
}

/*******************
** SET ENEMY SHIPS **
*******************/
void Game::set_enemy_ships_on_board(Board *pEnemyBoard, Ship *pEnemyShipFragmentssss){

    //Set the biggest ship, after that smaller and smaller
        //Generate random number, which is =>0 and <10
        //Check if field is free ('.')
        //Generate
    //At first, board is clean, and it is good occasion to put there the biggest ship.
    int temp_x, temp_y;
    bool plane; //"płaszczyzna" 1 if vertical, 0 horizontal
    int quantity_of_ships = 0;
    int how_many_ships_builded = 0;
    int position_of_ships_framgents_table = 0;

    //!!1 the biggest ship
        plane = int(rand() % 2); //random number in the range 0 - 1
        if(plane){ //vertical | y
            temp_x = rand() % 10+1; //random number in the range 1 - 10
            temp_y = rand() % 7+1; //random number in the range 1 - 7
            for(int i=0; i<4; i++, position_of_ships_framgents_table++){
                pEnemyBoard->set_field(temp_x, temp_y+i, '8');
                pEnemyShipFragments[position_of_ships_framgents_table].//!0-3
                set_parameters(1+how_many_ships_builded, temp_x, temp_y+i, 4, 0, plane);
            }                   //!1
        }else{ //horizontal - x
            temp_x = rand() % 7+1; //random number in the range 1 - 7
            temp_y = rand() % 10+1; //random number in the range 1 - 10
            for(int i=0; i<4; i++, position_of_ships_framgents_table++){
                pEnemyBoard->set_field(temp_x+i, temp_y, '8');
                pEnemyShipFragments[position_of_ships_framgents_table].//!0-3
                set_parameters(1+how_many_ships_builded, temp_x+i, temp_y, 4, 0, plane); //[0-3] position will be occupied by ship 1
            }                   //!1
        }
        how_many_ships_builded++; //!1

    //!!2 smaller ships
        while(quantity_of_ships < 2){
            while(true){//dopoki nie zbudujesz statku
                while(true){ //losuj wspolrzedne dopoki nie beda 'czyste'
                    temp_x = rand() % 8+1; //random number in the range 1 - 8
                    temp_y = rand() % 8+1; //random number in the range 1 - 8
                    if(isAreaClean(pEnemyBoard, temp_x, temp_y)) break;
                }
                //jezeli obszar jest czysty takze przy koncowce statku
                if(isAreaClean(pEnemyBoard, temp_x+2, temp_y)){
                    for(int i=0; i<3; i++, position_of_ships_framgents_table++){ //!
                        pEnemyBoard->set_field(temp_x+i, temp_y, '8');
                        pEnemyShipFragments[position_of_ships_framgents_table].
                            set_parameters(1+how_many_ships_builded, temp_x+i, temp_y, 3, 0, 0); //[4-6][7-9] position will be occupied by ships 2,3
                    }
                    break;
                }else if(isAreaClean(pEnemyBoard, temp_x, temp_y+2)){
                    for(int i=0; i<3; i++, position_of_ships_framgents_table++){
                        pEnemyBoard->set_field(temp_x, temp_y+i, '8');
                        pEnemyShipFragments[position_of_ships_framgents_table].
                            set_parameters(1+how_many_ships_builded, temp_x, temp_y+i, 3, 0, 1); //[4-6][7-9] position will be occupied by ship ships 2,3
                    }
                    break;
                }
            }
            quantity_of_ships++;
            how_many_ships_builded++;
        }

        quantity_of_ships = 0;
    //!!3 smaller ships
        while(quantity_of_ships < 3){
            while(true){//dopoki nie zbudujesz statku
                while(true){ //losuj wspolrzedne dopoki nie beda 'czyste'
                    temp_x = rand() % 9+1; //random number in the range 1 - 8
                    temp_y = rand() % 9+1; //random number in the range 1 - 8
                    if(isAreaClean(pEnemyBoard, temp_x, temp_y)) break;
                }

                //jezeli obszar jest czysty takze przy koncowce statku
                if(isAreaClean(pEnemyBoard, temp_x+1, temp_y)){
                    for(int i=0; i<2; i++, position_of_ships_framgents_table++){
                        pEnemyBoard->set_field(temp_x+i, temp_y, '8');
                        pEnemyShipFragments[position_of_ships_framgents_table].
                            set_parameters(1+how_many_ships_builded, temp_x+i, temp_y, 2, 0, 0); //[10-11][12-13][14-15] position will be occupied by ship ships 4,5,6
                    }
                    break;
                }else if(isAreaClean(pEnemyBoard, temp_x, temp_y+1)){
                    for(int i=0; i<2; i++, position_of_ships_framgents_table++){
                        pEnemyBoard->set_field(temp_x, temp_y+i, '8');
                        pEnemyShipFragments[position_of_ships_framgents_table].
                            set_parameters(1+how_many_ships_builded, temp_x, temp_y+i, 2, 0, 1);; //[10-11][12-13][14-15] position will be occupied by ship ships 4,5,6
                    }
                    break;
                }
            }
            quantity_of_ships++;
            how_many_ships_builded++;
        }

        quantity_of_ships = 0;
    //!!4 smallest ships
        while(quantity_of_ships<4){
            while(true){
                temp_x = rand() % 10+1; //random number in the range 1 - 8
                temp_y = rand() % 10+1; //random number in the range 1 - 8
                if(isAreaClean(pEnemyBoard, temp_x, temp_y)){
                    pEnemyBoard->set_field(temp_x, temp_y, '8');

                    pEnemyShipFragments[position_of_ships_framgents_table].
                            set_parameters(1+how_many_ships_builded, temp_x, temp_y, 1, 0, 0); //[16][17][18][19] position will be occupied by ship ships 7,8,9,10

                    if(position_of_ships_framgents_table<19){
                        position_of_ships_framgents_table++;
                    }
                    break;
                }
            }
            quantity_of_ships++;
            how_many_ships_builded++;
        }
}

/*******************
** SET PLAYER SHIPS **
*******************/
void Game::set_player_ships_on_board(Board *pPlayerBoard, Ship *pPlayerShipFragments){

    //Set the biggest ship, after that smaller and smaller
        //Generate random number, which is =>0 and <10
        //Check if field is free ('.')
        //Generate
    //At first, board is clean, and it is good occasion to put there the biggest ship.
    int temp_x, temp_y;
    bool plane; //"płaszczyzna" 1 if vertical, 0 horizontal
    int quantity_of_ships = 0;
    int how_many_ships_builded = 0;
    int position_of_ships_framgents_table = 0;
    int size_of_currently_builded_ship;

    //!!1 the biggest ship
    size_of_currently_builded_ship = 4;
    //player choosing plane
    plane = get_plane_from_player(size_of_currently_builded_ship);

    cout<<"Podaj wspolrzedna x do ustawienia statku: " << endl;
    temp_x = Game::get_cordinates(plane?10:7);

    cout<<"Podaj wspolrzedna y do ustawienia statku : " << endl;
    temp_y = Game::get_cordinates(plane?7:10);

    if(plane){ //vertical | y
        for(int i=0; i<size_of_currently_builded_ship; i++, position_of_ships_framgents_table++){
            pPlayerBoard->set_field(temp_x, temp_y+i, '8');
            pPlayerShipFragments[position_of_ships_framgents_table].//!0-3
            set_parameters(1+how_many_ships_builded, temp_x, temp_y+i, size_of_currently_builded_ship, 0, plane);
        }                   //!1
    }else{ //horizontal - x
        for(int i=0; i<size_of_currently_builded_ship; i++, position_of_ships_framgents_table++){
            pPlayerBoard->set_field(temp_x+i, temp_y, '8');
            pPlayerShipFragments[position_of_ships_framgents_table].//!0-3
            set_parameters(1+how_many_ships_builded, temp_x+i, temp_y, size_of_currently_builded_ship, 0, plane); //[0-3] position will be occupied by ship 1
        }                   //!1
    }
    how_many_ships_builded++; //!1
    pPlayerBoard->print_board(1);

    //!!2 smaller ships
    size_of_currently_builded_ship = 3;
    while(quantity_of_ships < 2){
        while(true){//dopoki nie zbudujesz statku
            while(true){ //sczytuj wspolrzedne dopoki nie beda 'czyste'
                plane = get_plane_from_player(size_of_currently_builded_ship);
                cout<<"Podaj wspolrzedna x do ustawienia statku: " << endl;
                temp_x = Game::get_cordinates(plane?10:8);
                cout<<"Podaj wspolrzedna y do ustawienia statku : " << endl;
                temp_y = Game::get_cordinates(plane?8:10);

                if( (isAreaClean(pPlayerBoard, temp_x, temp_y)) && (isAreaClean(pPlayerBoard, (plane?temp_x:(temp_x+2)), (plane?(temp_y+2):temp_y))) )
                    break;
            }

            if(!plane){
                for(int i=0; i<size_of_currently_builded_ship; i++, position_of_ships_framgents_table++){ //!
                    pPlayerBoard->set_field(temp_x+i, temp_y, '8');
                    pEnemyShipFragments[position_of_ships_framgents_table].
                        set_parameters(1+how_many_ships_builded, temp_x+i, temp_y, size_of_currently_builded_ship, 0, 0); //[4-6][7-9] position will be occupied by ships 2,3
                }
                break;
            }else{
                for(int i=0; i<size_of_currently_builded_ship; i++, position_of_ships_framgents_table++){
                    pPlayerBoard->set_field(temp_x, temp_y+i, '8');
                    pEnemyShipFragments[position_of_ships_framgents_table].
                        set_parameters(1+how_many_ships_builded, temp_x, temp_y+i, size_of_currently_builded_ship, 0, 1); //[4-6][7-9] position will be occupied by ship ships 2,3
                }
                break;
            }
        }
        quantity_of_ships++;
        how_many_ships_builded++;
        pPlayerBoard->print_board(1);
    }

    quantity_of_ships = 0;
    //!!3 smaller ships
    size_of_currently_builded_ship = 2;
    while(quantity_of_ships < 3){
        while(true){//dopoki nie zbudujesz statku
            while(true){ //sczytuj wspolrzedne dopoki nie beda 'czyste'
                plane = get_plane_from_player(size_of_currently_builded_ship);
                cout<<"Podaj wspolrzedna x do ustawienia statku: " << endl;
                temp_x = Game::get_cordinates(plane?10:9);
                cout<<"Podaj wspolrzedna y do ustawienia statku : " << endl;
                temp_y = Game::get_cordinates(plane?9:10);

                if( (isAreaClean(pPlayerBoard, temp_x, temp_y)) && (isAreaClean(pPlayerBoard, (plane?temp_x:(temp_x+1)), (plane?(temp_y+1):temp_y))) )
                    break;
            }

            if(!plane){
                for(int i=0; i<size_of_currently_builded_ship; i++, position_of_ships_framgents_table++){ //!
                    pPlayerBoard->set_field(temp_x+i, temp_y, '8');
                    pEnemyShipFragments[position_of_ships_framgents_table].
                        set_parameters(1+how_many_ships_builded, temp_x+i, temp_y, size_of_currently_builded_ship, 0, 0); //[4-6][7-9] position will be occupied by ships 2,3
                }
                break;
            }else{
                for(int i=0; i<size_of_currently_builded_ship; i++, position_of_ships_framgents_table++){
                    pPlayerBoard->set_field(temp_x, temp_y+i, '8');
                    pEnemyShipFragments[position_of_ships_framgents_table].
                        set_parameters(1+how_many_ships_builded, temp_x, temp_y+i, size_of_currently_builded_ship, 0, 1); //[4-6][7-9] position will be occupied by ship ships 2,3
                }
                break;
            }
        }
        quantity_of_ships++;
        how_many_ships_builded++;
        pPlayerBoard->print_board(1);
    }

    quantity_of_ships = 0;
    //!!4 smallest ships
    while(quantity_of_ships<4){
        while(true){
            plane = 0; //ship has only 1 field so it can has plane 0 or 1
            cout<<"Podaj wspolrzedna x do ustawienia statku: " << endl;
            temp_x = Game::get_cordinates(10);
            cout<<"Podaj wspolrzedna y do ustawienia statku : " << endl;
            temp_y = Game::get_cordinates(10);
            if(isAreaClean(pPlayerBoard, temp_x, temp_y)){
                pPlayerBoard->set_field(temp_x, temp_y, '8');

                pPlayerShipFragments[position_of_ships_framgents_table].
                        set_parameters(1+how_many_ships_builded, temp_x, temp_y, 1, 0, 0); //[16][17][18][19] position will be occupied by ship ships 7,8,9,10

                if(position_of_ships_framgents_table<19){
                    position_of_ships_framgents_table++;
                }
                break;
            }
        }
        quantity_of_ships++;
        how_many_ships_builded++;
        pPlayerBoard->print_board(1);
    }
    system("cls");
    cout<<"Koniec funkcji set_player_ships_on_board  WCISNIJ DOWOLNY KLAWISZ!!!"<<endl<<endl;
    getch();
}

/*******************
** CHECK IS AREA CLEAN **
*******************/
bool Game::isAreaClean(Board *pEnemyBoard, int temp_x, int temp_y){

 if(((pEnemyBoard->get_field(temp_x-1, temp_y-1) == '.') || (pEnemyBoard->get_field(temp_x-1, temp_y-1) == '*')) &&
    ((pEnemyBoard->get_field(temp_x, temp_y-1) == '.')   || (pEnemyBoard->get_field(temp_x, temp_y-1) == '*')) &&
    ((pEnemyBoard->get_field(temp_x+1, temp_y-1) == '.') || (pEnemyBoard->get_field(temp_x+1, temp_y-1) == '*')) &&
    ((pEnemyBoard->get_field(temp_x-1, temp_y) == '.')   || (pEnemyBoard->get_field(temp_x-1, temp_y) == '*')) &&
    ((pEnemyBoard->get_field(temp_x, temp_y) == '.')     || (pEnemyBoard->get_field(temp_x, temp_y) == '*')) &&
    ((pEnemyBoard->get_field(temp_x+1, temp_y) == '.')   || (pEnemyBoard->get_field(temp_x+1, temp_y) == '*')) &&
    ((pEnemyBoard->get_field(temp_x-1, temp_y+1) == '.') || (pEnemyBoard->get_field(temp_x-1, temp_y+1) == '*')) &&
    ((pEnemyBoard->get_field(temp_x, temp_y+1) == '.')   || (pEnemyBoard->get_field(temp_x, temp_y+1) == '*')) &&
    ((pEnemyBoard->get_field(temp_x+1, temp_y+1) == '.') || (pEnemyBoard->get_field(temp_x+1, temp_y+1) == '*')))
        return true;
    else return false;
}

/*******************
** GET PLANE FROM PLAYER **
*******************/
bool Game::get_plane_from_player(int howBigIsIt ){
    char choose;
    while(true){
        cout<<"Podaj plaszczyzne ustawienia "<<howBigIsIt<<"masztowca (1 - pion, 0 - poziom)"<<endl;
        choose = getch();
        if(choose == '1'){
            return true;
            break;
        }else if(choose == '0'){
            return false;
            break;
        }
    }
}

/*******************
** GET CORDINATES **
*******************/
short int Game::get_cordinates(int maximum){

    int temp;

    do{
        cin>>temp;
        if(temp<1 || temp>maximum) cout << "Blad, jeszcze raz!" <<endl;
        else break;
    }while(true);

    return temp;
}
/*******************
** GETs/ SETs **
*******************/
void Game::set_board_visibility(int is_board_visible){
    this->is_board_visible=is_board_visible;
}
bool Game::get_board_visibility(){
    return is_board_visible;
}

/*******************
** DESTRUCTOR **
*******************/
Game::~Game()
{
    delete [] pEnemyShipFragments;
    pEnemyShipFragments = NULL;
}
