#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <conio.h>
#include <time.h>

#include "Board.h"

using namespace std;

/*******************
** CONSTRUCTOR **
*******************/
Board::Board()
{
    boardArray = new char * [length_of_board];
    for(int i=0;i<length_of_board;i++){
        boardArray[i] = new char [length_of_board];
    }
    initialization(boardArray, length_of_board);
}

/*******************
** DESTRUCTOR **
*******************/
Board::~Board()
{
    for(int i=0;i<length_of_board;i++)
        delete [] boardArray[i];
    delete [] boardArray;
    boardArray = NULL;
}

/*******************
** INITIALIZATION **
*******************/
void Board::initialization(char **boardArray, const int length_of_board){

    for(int i=0;i<length_of_board;i++){
        for(int j=0;j<length_of_board;j++){
            boardArray[i][j]='.';
        }
    }
}

/*******************
** PRINT BOARD **
*******************/
void Board::print_board(bool visibility){

    cout<<"_  ";
    for(int i=1;i<length_of_board-1;i++){
		cout<<i<<"  ";
	}
	cout<<endl;

    for(int i=1;i<length_of_board-1;i++){
        cout<<i;
        for(int j=1;j<length_of_board-1;j++)
            if(visibility) cout<<(((i>9)&&(j<2))?" ":"  ")<<boardArray[i][j]; //if player want to see enemy`s ships
            else{
                cout<<(((i>9)&&(j<2))?" ":"  ")<<((boardArray[i][j]=='8')?'.':boardArray[i][j]);
            }

        cout<<endl;
    }
    cout<<"___________________________"<<endl;
}

/*******************
** GET / SET **
*******************/
char Board::get_field(int x, int y){

    return boardArray[y][x]; //y must be first
}

void Board::set_field(int x, int y, char field){
    boardArray[y][x] = field; //y must be first
}

const int Board::get_length_of_board(){
    return length_of_board;
}
