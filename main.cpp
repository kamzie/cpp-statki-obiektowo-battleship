//Autor: Kamil Zieliñski
//email: kamil.zielinski.1992@gmail.com
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
#include <conio.h>
#include <time.h>
#include <math.h>
#include "Game.h"

using namespace std;

/*******************
** CLASS MENU **
*******************/
class Menu{
public:
    Menu(){
        printMenu();
    }
    void printMenu();
    void about();

};

/*******************
**     MAIN     **
*******************/
int main()
{
    Menu menu;
}

/*******************
** MENU PRINT MENU() **
*******************/
void Menu::printMenu(){
    int option=0;
    do{
        system("cls");
        cout << "**** Ericpol Task 4 - Object Oriented Version ****" << endl;
        cout << "Menu: " << endl;
        cout << "\t 1. Game " << endl << "\t 2. About"<< endl << "\t 3. Quit"<< endl;
        cin >> option;

        switch(option)
        {
            case 1:
            {
                system("cls");
                Game game;
                break;
            }
            case 2:
            {
                system("cls");
                about();
                break;
            }
            case 3:
            {
                system("cls");
                cout<<endl<<"\t****Option 3 [Quit]"<<endl;
                break;
            }
            default:
            {
                cout<<"\tError, try again!"<<endl;
                system("cls");
                break;
            }
            break;
        }
        break;
    }while(option!=3);
}

/*******************
** MENU ABOUT() **
*******************/
void Menu::about(){

    cout<<"Autorem aplikacji jest Kamil Zielinski"<<endl;
    cout<<"email: kamil.zielinski.1992@gmail.com"<<endl;
    if(getch()){printMenu();}
}
