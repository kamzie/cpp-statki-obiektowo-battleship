/* Class Board is responsible for:
- creating Board (arrary):
    - creating two-dimensional array
    - initialization this array '.' characters
    - printing board
    - programming interface (get/set)
*/
#ifndef BOARD_H
#define BOARD_H

class Board
{
    public:
        Board();
        virtual ~Board();

        void initialization(char **boardArray, const int length_of_board);
        void print_board(bool visibility);
        char get_field(int x, int y);
        void set_field(int x, int y, char field);
        const int get_length_of_board();

    protected:
    private:
        char **boardArray;
        const int length_of_board = 12; //10 + 2 becouse 10 is for board and 2 for invisible fields
};

#endif // BOARD_H
