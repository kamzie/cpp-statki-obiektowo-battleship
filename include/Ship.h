#ifndef SHIP_H
#define SHIP_H

class Ship
{
    public:
        Ship(); //default constructor initialize everything by 0
        void set_parameters(int id, int x, int y, int length_of_ship, int damage, int plane);
        void print_data();
        virtual ~Ship();
        //get set
        int get_id();
        void set_id(int id);
        int get_x();
        void set_x(int x);
        int get_y();
        void set_y(int y);
        int get_length();
        void set_length(int length);
        int get_damage();
        void set_damage(int damage);
        void increase_damage();

        int get_plane();
        void set_plane(int plane);

    protected:
    private:
        int id;
        int x;
        int y;
        int length;
        int damage;
        int plane;
        //char state; //1 exist, 2 damage, 3 destroyed
};

#endif // SHIP_H
