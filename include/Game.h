/*Class Game is responsible for
 - console i/o
 - creating and calling board object
 - gaming loop
 - set ships on board
 - shooting and collision detecting
 */

#ifndef GAME_H
#define GAME_H
#include "Board.h"
#include "Ship.h"

class Game
{
    public:
        Game();
        virtual ~Game();

        void game_loop (Board *pEnemyBoard, Ship *pEnemyShipFragments, Board *pPlayerBoard, Ship *pPlayerShipFragments);

        void player_shooting(Board *pEnemyBoard, Ship *pEnemyShipFragments);
        void computer_shooting(Board *pPlayerBoard, Ship *pPlayerShipFragments);

        void set_enemy_ships_on_board(Board *pEnemyBoard, Ship *pEnemyShipFragments);
        void set_player_ships_on_board(Board *pPlayerBoard, Ship *pPlayerShipFragments);

        void print_ships_data(Ship *pEnemyShipFragments, int how_many_fragments);
        short int get_cordinates(int maximum); //get from player and check cordinates
        bool isAreaClean(Board *pEnemyBoard, int x, int y);
        int detect_which_fragment_was_hited(int x, int y, Ship *pEnemyShipFragments, int how_many_fragments);
        void increase_damage(Ship *pEnemyShipFragments, int id);
        void exclude_positions(Board *pEnemyBoard, Ship *pEnemyShipFragments, int which_fragment, int id);
        bool get_plane_from_player(int howBigIsIt);
        //gety sety
        void set_board_visibility(int is_board_visible);
        bool get_board_visibility();
        void increment_hits();
        short int get_how_many_hits();



    protected:
    private:

        static const int how_many_ships = 10;
        static const int how_many_fragments = 20;
        Board *pEnemyBoard = new Board(); //Array contains enemy`s ships
        Board *pPlayerBoard = new Board();
        Ship *pEnemyShipFragments = new Ship[how_many_fragments]; // rezerwujemy obszar pami�ci
        Ship *pPlayerShipFragments = new Ship[how_many_fragments];
        bool is_board_visible; //responsible for enemy ships visibility, default true

        static int how_many_hits_made_player;
        static int how_many_hits_made_computer;


};

#endif // GAME_H
